#!/usr/bin/python

import sys
import os
import argparse
import math

import quantities as pq
import matplotlib.pyplot as plt
import numpy as np
import plotly.offline as off
import plotly.figure_factory as ff
import plotly.graph_objs as go

from collections import Counter
from mpmath import mp

##---------------------------------------------------------------
## Notes
##---------------------------------------------------------------

# - Project based on Cristian Ilies Vasile's "The Flow of Pi"
# - Plotly tutorial on Chord Diagrams: https://plot.ly/python/filled-chord-diagram/
# - Configuration: edit CONFIG dictionary below
#       - Most relevant parameters:
#           - num_digits, color_map, width_mm, height_mm and margins_mm, R_ribb_th
# - To draw values other than pi,
#       - substitute mp.pi in genPiVector()
#       - ensure correct number of digits by using something similar to
#           mp.dps = CONFIG['num_digits'] in checkConfig()


##---------------------------------------------------------------
## Declarations / Basic Definitions / Constants
##---------------------------------------------------------------

# Pi
PI=np.pi

CONFIG = {
    # Number of digits of pi to be evaluated
    'num_digits': 10000,
    # 'num_digits': 2000,

    # Gap between two consecutive ideograms [rad]
    'ii_gap': 2*PI * 0.005,

    # Colormap and alpha value used for ideograms and ribbons
    #   https://matplotlib.org/examples/color/colormaps_reference.html
    #   Colormap offset can be used to skip black at start of a color map
    # 'color_map': ['terrain', 0],
    'color_map': ['nipy_spectral', 1],
    # 'color_map': ['RdYlBu', 0],
    # 'color_map': ['viridis', 0],
    'ideo_color_alpha': 1,
    'ribbon_color_alpha': 0.25,

    # Color and width of ideogram outlines
    'color_iborder': 'rgb(255,255,255)',
    'width_iborder': 3,

    # Output filename
    'filename': 'chord-diagram-pi.html',

    # Figure title
    'title': '', # 'The Flow of Pi',

    # Figure dimentions
    # 'width_mm': 500,
    # 'height_mm': 700,
    'width_mm': 100,
    'height_mm': 110,
    # 'margins_mm': 15,   # Top, bottom, left and right margins
    'margins_mm': 3,
    'dpi': 300,         # Resolution

    # Range for x and y axes (-value to value)
    #   Depends on figure size, e.g. 1.1 works well for width of 1600 pixels
    'axis_range': 1.1,

    # Annotation color and text size
    #   Note that font sizes will be scaled with figure width, base values chosen for 1600px width
    'annot_color': 'rgb(255,255,255)',
    'annot_color_match': False,  # Match label color to ideograms
    'annot_size_labels': 42,
    'annot_size_pi': 160,
    'annot_size_signature': 20,

    # Background color
    'bgcolor': 'rgba(0,0,0,1)', # Black

    # Radii for ideogram and ribbon boundaries
    'R_labels': 1.1,
    'R_ideo_outer': 1.05,
    'R_ideo_inner': 1.00,
    'R_ribb_outer': 0.95,

    # Radius used to align mid points of ribbons
    #   Up to 4 separate levels at R_ribb_th, R_ribb_th^2, R_ribb_th^3 and R_ribb_th^4
    #   Value needs to be < 1
    'R_ribb_th': 0.6,
    # 'R_ribb_th': 0.65,
    # 'R_ribb_th': 1-1/PI,
}


##---------------------------------------------------------------
## Classes / Functions
##---------------------------------------------------------------


class Dimensions(object):
    """ Helper class holding information on image size, resolution etc. """

    def __init__(self, width_mm, height_mm, margins_mm, dpi):
        """ Input size and margins in mm, specify dpi """
        self.width_px =  math.ceil(dpi * self.mmToIn(width_mm))
        self.height_px =  math.ceil(dpi * self.mmToIn(height_mm))
        self.margins_px =  math.ceil(dpi * self.mmToIn(margins_mm))

        self.hw_ratio = self.height_px / self.width_px

    def mmToIn(self, lenght_mm):
        """ Convert lenght from mm to inch """
        lenght_in = lenght_mm * pq.mm
        lenght_in.units = 'in'
        return lenght_in


def checkConfig():
    """ Verify config options """
    # Set number of digits of pi
    mp.dps = CONFIG['num_digits']

    # Compute figure dimensions in pixels
    CONFIG['dim'] = Dimensions(CONFIG['width_mm'],
                               CONFIG['height_mm'],
                               CONFIG['margins_mm'],
                               CONFIG['dpi'])
    # Scale font sizes
    for key in ['annot_size_labels', 'annot_size_pi', 'annot_size_signature']:
        CONFIG[key] *= CONFIG['dim'].width_px / 1600
    return


def moduloAB(x, a, b):
    """ Maps a real number onto the unit circle identified with  the interval [a,b), b-a=2*PI """
    if a >= b:
        raise ValueError('Incorrect interval ends')
    y = (x - a) % (b - a)
    return y + b if y < 0 else y + a


def test2PI(x):
    """ Check that argument is in range 0 to 2*Pi """
    return 0 <= x < 2 * PI


def correctRange(x):
    """ Add 2*pi offset to angular coordinates < 0 """
    return (x if x >= 0 else x + 2*PI)


def getIdeogramEnds(ideogram_len):
    """ Return list of end angular coordinates for each ideogram arc """
    # Range 0 ... 2*pi
        # Assign coordinates in decreasing order so that ideograms are drawn clockwise,
        # rather than counter clockwise
    # 0 ... is center right position of the plot
        # Use pi/2 offset to move start to top center position
        # Need to correct for resulting values > 2*pi
    ideo_ends=[]
    right=PI/2  # Offset to move start to top center

    for k in range(len(ideogram_len)):
        # Move right to left (decreasing angular coordinates) to draw ideograms clockwise
        left = correctRange(right - ideogram_len[k])
        ideo_ends.append([left, right])
        right = correctRange(left - CONFIG['ii_gap'])
    return ideo_ends


def genIdeogramArc(R, phi, a=50):
    """ Return equally spaced points on an ideogram arc (as complex numbers in polar form) """
    # R is the circle radius
    # phi is the list of ends angle coordinates of an arc
    # a is a parameter that controls the number of points to be evaluated on an arc
    if not test2PI(phi[0]) or not test2PI(phi[1]):
        phi = [moduloAB(t, 0, 2 * PI) for t in phi]
    length = (phi[1] - phi[0]) % 2 * PI
    nr = 5 if length <= PI / 4 else int(a * length / PI)

    if phi[0] < phi[1]:
        theta = np.linspace(phi[0], phi[1], nr)
    else:
        phi = [moduloAB(t, -PI, PI) for t in phi]
        theta = np.linspace(phi[0], phi[1], nr)

    return R*np.exp(1j * theta)


def genLayout():
    """ Define data and layout for the Plotly plot of the chord diagram """
    axis = dict(showline = False, # hide axis line, grid, ticklabels and  title
          zeroline = False,
          showgrid = False,
          showticklabels = False,
          title = ''
          )

    margins = CONFIG['dim'].margins_px
    return go.Layout(title = CONFIG['title'],
                  xaxis = dict(axis),
                  yaxis = dict(axis),
                  showlegend = False,
                  width = CONFIG['dim'].width_px,
                  height = CONFIG['dim'].height_px,
                  margin = dict(t = margins, b = margins,
                                l = margins, r = margins),
                  hovermode = 'closest',
                 )


def genIdeoShape(path, fill_color):
    """ Return the Plotly shape of an ideogram """
    return  dict(
                  line = dict(
                  color = CONFIG['color_iborder'],
                  width = CONFIG['width_iborder'],
                 ),
            path = path,
            type = 'path',
            fillcolor = fill_color,
            layer = 'below'
        )


def genRadiusShape(radius, angle, color = 'rgb(50, 171, 96)'):
    """ Return line starting from 0,0 with given radius and angle """
    line = radius * np.exp(1j*angle)
    return {
        'type': 'line',
        'line': {'color': color, 'width': 4},
        'x0': 0,
        'y0': 0,
        'x1': line.real,
        'y1': line.imag,
    }


def genHintShape(position, line_color, text):
    """ Return the Plotly shape of a "hint" text on hovering """
    return  go.Scatter(x = position.real,
                       y = position.imag,
                       mode = 'lines',
                       line = dict(color = line_color, shape = 'spline', width = 1),
                       text = text,
                       hoverinfo = 'text',
                       opacity = 0,
                       )


def genRibbonShape(start, end, ctrl, line_color, ribbon_width):
    """ Return a Quadratic Bezier Curve describing a ribbon between a start and an end point"""
    # Describe path of the ribbon
    # 'M 0,1 Q 0,0 1,0' ... start, conrol, end; format real,imaginary
    path = 'M {0},{1} Q {2},{3} {4},{5}'.format(
                start.real, start.imag, # Start point
                ctrl.real,ctrl.imag,    # Control point
                end.real, end.imag)     # End point

    # Convert ribbon width to line width
    # Use slightly smaller value to allow gaps between lines
    line_width = ribbon_width * CONFIG['dim'].width_px / (1.3*PI*CONFIG['R_ribb_outer'])

    return  {'line': {
                'color': line_color,
                'width': line_width
             },
             'path': path,
             'type': 'path',
             'layer': 'below'
        }


def getIdeoColors(count, alpha):
    """ Extract list of colors from colormap and update alpha value """
    offset = CONFIG['color_map'][1] # Can be used to skip black at start of color map
    max = count + 2 + offset # Avoid extreme ends of the map
    cmap = plt.cm.get_cmap(CONFIG['color_map'][0])
    colors = []
    for i in range(count):
        c = np.array(cmap((i + offset) / max)) * 255   # Convert 0-1 value to 0-255 value
        c = c.astype(int)   # Round down
        c = 'rgba({:d}, {:d}, {:d}, {:f})'.format(c[0], c[1], c[2], alpha)
        colors.append(c)

    return colors


def genIdeograms(pi_vector, digit_labels, digit_counts, ribbon_width):
    """ Generate Ideograms (ring outline) """
    # Lenght of each ideogram is multiple of ribbon width
    ideogram_lengths = ribbon_width * np.asarray(digit_counts)

    # List of end angular coordinates for each ideogram arc
    ideo_ends = getIdeogramEnds(ideogram_lengths)

    # List of colors for ideograms and ribbons
    ideo_colors = getIdeoColors(len(digit_labels), CONFIG['ideo_color_alpha'])

    # Specify data ("hint" text on hovering) and layout for each ideogram
    shapes = []
    data = []
    for k in range(len(ideo_ends)):
        z = genIdeogramArc(CONFIG['R_ideo_outer'], ideo_ends[k])
        zi = genIdeogramArc(CONFIG['R_ideo_inner'], ideo_ends[k])
        m = len(z)
        n = len(zi)

        hint_text = 'Number {0}<br>{1} occurances'.format(digit_labels[k], digit_counts[k])
        data.append(genHintShape(z, ideo_colors[k], hint_text))

        path = 'M '
        for s in range(m):
            path += str(z.real[s])+', '+str(z.imag[s])+' L '

        Zi = np.array(zi.tolist()[::-1])

        for s in range(m):
            path += str(Zi.real[s])+', '+str(Zi.imag[s])+' L '
        path += str(z.real[0])+' ,'+str(z.imag[0])

        shapes.append(genIdeoShape(path, ideo_colors[k]))

    return data, shapes, ideo_ends


def insertDummies(data, labels, max_digit):
    """ Insert dummy values into digit related data for digits with 0 occurances """
    result = [None] * max_digit
    for label, datum in zip(labels, data):
        result[label] = datum
    return result


def angleToRI(theta, R):
    """ Convert angle theta and radius R to coordinates, return real and imaginary components """
    return R*np.exp(1j * correctRange(theta))


def getCtrlPlt(start, end):
    """ Adjust control point of the Quadratic Bezier Curve so that its mid point lies on one of
        4 discrete radii; result are 4 circles formed by the mid points of all ribbons
    """
    # Quadratic Bezier Curve
    #   B(t) = (1-t)^2 * P0 + 2(1-t)t * P1 + t^2 * P2 , t = 0..1
    #       re = (1 - t) * (1 - t) * P0.re + 2 * (1 - t) * t * P1.re + t * t * P2.re
    #       im = (1 - t) * (1 - t) * P0.im + 2 * (1 - t) * t * P1.im + t * t * P2.im
    #   P0.. start point
    #   P1.. control point -> default 0,0 chosen for the center
    #   P2.. end point
    #   t.. parameter -> 0.5 in the middle of the curve

    # Compute mid point (t = 0.5) for control point at 0,0
    mid_old = 0.25*(start.real + end.real) + 1j*0.25*(start.imag + end.imag)
    R_mid_old = abs(mid_old)

    assert((CONFIG['R_ribb_th'] < 1) and (CONFIG['R_ribb_th'] > 0))
    R_ribb = CONFIG['R_ribb_th']

    # Adjust control point radius based on old mid point radius
    #   Threshold values chosen empirically (showed up naturally for num_digits >= 1000)
    if R_mid_old > 0.4:
        R_mid_new = R_ribb
    elif R_mid_old > 0.3:
        R_mid_new = R_ribb*R_ribb
    elif R_mid_old > 0.2:
        R_mid_new = R_ribb*R_ribb*R_ribb
    else:
        R_mid_new = R_ribb*R_ribb*R_ribb*R_ribb

    # Calculate new control point so that new mid point has same angle
    mid_new = R_mid_new * np.exp(1j*np.angle(mid_old))
    ctrl_re = 2*(mid_new.real - 0.25*(start.real + end.real))
    ctrl_im = 2*(mid_new.imag - 0.25*(start.imag + end.imag))

    return ctrl_re + 1j*ctrl_im # New control point


def genRibbons(pi_vector, ideo_ends, ribbon_width, digit_labels):
    """ Generate Ribbons (lines connecting the outer rings) """
    shapes = []

    # DEBUG: guidelines of lenght R_ribb_inner from center
    # for i in range(8):
        # shapes.append(genRadiusShape(CONFIG['R_ribb_inner'], i*PI/4))

        # shapes.append(genRadiusShape(0.38, i*PI/4, color = 'white'))
        # shapes.append(genRadiusShape(CONFIG['R_ribb_inner'], i*PI/4))

        # shapes.append(genRadiusShape(0.45, i*PI/4+PI/8, color = 'red'))
        # shapes.append(genRadiusShape(0.29, i*PI/4+PI/8, color = 'white'))

    # Value of the highest digit found in the input vector
    max_digit = digit_labels[-1] + 1

    # Starting angle for each ideogram [in rad], insert dummy values for digits with 0 occurances
    # Offset by half the width of a ribbon
    digit_angle = insertDummies([i[1] - ribbon_width/2 for i in ideo_ends], digit_labels, max_digit)

    # Ribbon colors based on ideogram colors, insert dummy values for digits with 0 occurances
    ideo_colors = getIdeoColors(len(digit_labels), CONFIG['ribbon_color_alpha'])
    ribbon_colors = insertDummies(ideo_colors, digit_labels, max_digit)

    # Loop through input vector, connecting current to next digit with a ribbon
    cur_digit = pi_vector[0]
    for nxt_digit in pi_vector[1:]:

        # Get real and imaginary parts from angles corresponding to current and next digit
        cur = angleToRI(digit_angle[cur_digit], CONFIG['R_ribb_outer'])
        nxt = angleToRI(digit_angle[nxt_digit], CONFIG['R_ribb_outer'])

        # Update starting position for next ribbon with same current digit
        digit_angle[cur_digit] -= ribbon_width

        # Move control point of the curve, if needed
        ctrl_point = getCtrlPlt(cur, nxt)

        # DEBUG: only show lines that would have crossed R_ribb_inner
        # if abs(ctrl_point) == 0:
            # cur_digit = nxt_digit
            # continue

        # Add shape for the ribbon
        shapes.append(genRibbonShape(cur, nxt, ctrl_point, ribbon_colors[cur_digit], ribbon_width))
        cur_digit = nxt_digit

        # DEBUG
        # break

    return shapes


def genPiVector():
    """ Generate input vector and count digit occurances """
    # Input vector (list of Pi's digits without the '.')
    pi_vector = '3' + str(mp.pi)[2:]
    pi_vector = np.array(list(pi_vector), dtype = int)

    # Count occurances of non-negative values; reverse result to enable clockwise display later
    _digit_counts = np.bincount(pi_vector)

    digit_labels = []   # Name (label) of each digit
    digit_counts = []   # Occurances of each digit

    # Ignore digits with 0 occurances
    for l, bc in enumerate(_digit_counts):
        if bc == 0:
            continue
        digit_labels.append(l)
        digit_counts.append(bc)

    return pi_vector, digit_labels, digit_counts


def getRibbonWidth(digit_labels):
    """ Calculate width of a ribbon """
    # (2 * Pi - gap_width * num_gaps) / num_digits
    return (2 * PI - CONFIG['ii_gap'] * len(digit_labels)) / CONFIG['num_digits']


def getXYFactors():
    """ Return correction factor for 'x' or 'y' axis based on figures height-widht ratio """
    hw_ratio = CONFIG['dim'].hw_ratio
    x_factor = 1/hw_ratio if hw_ratio < 1 else 1
    y_factor = hw_ratio if hw_ratio > 1 else 1
    return x_factor, y_factor


def getAxisLengths():
    """ Return manually fixed axes ranges to avoid undesired stretching """
    axis = dict(
                showgrid=False,
                zeroline=False,
                showline=False,
                ticks='',
                showticklabels=False
            )
    # If height-widht ratio != 1, one axis will be compressed by the layout's margins
    # Correct axis to avoid distortions
    offset_margin = CONFIG['dim'].margins_px/CONFIG['dim'].width_px
    x_offset = offset_margin if CONFIG['dim'].hw_ratio < 1 else 0
    y_offset = offset_margin if CONFIG['dim'].hw_ratio > 1 else 0

    # Increase height or width of the figure based on the heiht-widht ratio
    x_factor, y_factor = getXYFactors()

    xaxis = {**axis, **{'range': [-CONFIG['axis_range']*x_factor-x_offset,
                                   CONFIG['axis_range']*x_factor+x_offset]}}
    yaxis = {**axis, **{'range': [-CONFIG['axis_range']*y_factor-y_offset,
                                   CONFIG['axis_range']*y_factor+y_offset]}}
    return xaxis, yaxis


def genFigure(data, shapes, annotations):
    """ Generate output figure """
    # Configure the layout
    layout = genLayout()
    layout['shapes'] = shapes
    layout['annotations'] = annotations

    # Manually fix axes ranges to avoid undesired stretching
    xaxis, yaxis = getAxisLengths()
    layout['xaxis'] = xaxis
    layout['yaxis'] = yaxis

    # Set background color
    layout['paper_bgcolor'] = CONFIG['bgcolor']
    layout['plot_bgcolor'] = CONFIG['bgcolor']

    fig = go.Figure(data = data, layout = layout)
    off.plot(fig, filename = CONFIG['filename'])
    # off.plot(fig, filename = CONFIG['filename'], image = 'svg', image_filename=CONFIG['filename'])


def genSignature():
    """ Return signature related annotations """
    result = []
    annot_sig = {
            'xref': 'paper',
            'yref': 'paper',
            'font': {
                'family': 'Times New Roman',
                'size': CONFIG['annot_size_signature'],
                'color': CONFIG['annot_color'],
            },
            'showarrow': False,
            'xanchor': 'right',
            'yanchor': 'bottom',
    }
    # Insert personal signature
    #   Read author's name from file
    author_file = 'author.txt'
    if os.path.exists(author_file):
        with open(author_file, 'r') as f:
            result.append({**annot_sig, **{
                    'x': 1,
                    'y': CONFIG['annot_size_signature']/CONFIG['dim'].width_px*getXYFactors()[0],
                    'text': f.readline() + ', 2019 ',
            }})
    result.append({**annot_sig, **{
            'x': 1,
            'y': 0,
            'text': 'Based on Cristian Ilies Vasile\'s "The Flow of Pi"',
    }})
    return result


def genAnnotations(digit_labels, ideo_ends):
    """ Generate annotations (labels around the edge, central Pi symbol, ...) """
    annotations = []

    annot_def = {
        'showarrow': False,
        'xref': 'x',
        'yref': 'y',
        'xanchor': 'center',
        'yanchor': 'middle',
    }
    ideo_colors = getIdeoColors(len(digit_labels), CONFIG['ideo_color_alpha'])

    # Label on outside of each ideogram
    for color, label, [start, end] in zip(ideo_colors, digit_labels, ideo_ends):
        # Correct end when passing 0 to 2*PI
        end = end if (start < end) else end + 2*PI
        # Angle between start and end of the ideogram
        angle = (start + end)/2
        pos = angleToRI(angle, CONFIG['R_labels'])

        annot_cur = {
            'x': pos.real,
            'y': pos.imag,
            'text': '_{0}_'.format(label),
            'textangle': 90-math.degrees(angle),
            'font': {
                'family': 'Courier New, monospace',
                'size': CONFIG['annot_size_labels'],
                'color': color if CONFIG['annot_color_match'] else CONFIG['annot_color'],
            }
        }
        annotations.append({**annot_def, **annot_cur})

    # Pi symbol in the center
    annot_pi = {
            'x': 0,
            # Add vertical offset to center the lower case character
            'y': 0.4*CONFIG['annot_size_pi']/CONFIG['dim'].height_px,
            'text': u"\u03C0",
            'font': {
                'family': 'Times New Roman',
                'size': CONFIG['annot_size_pi'],
                'color': CONFIG['annot_color'],
            },
    }
    annotations.append({**annot_def, **annot_pi})

    # Signature
    annotations.extend(genSignature())
    return annotations


##---------------------------------------------------------------
## Main
##---------------------------------------------------------------

if __name__ == '__main__':
    # Evaluate command line options
    checkConfig()

    # Generate input vector (list of Pi's digits without the '.')
    pi_vector, digit_labels, digit_counts = genPiVector()
    # print(pi_vector)

    # Get width of a ribbon
    ribbon_width = getRibbonWidth(digit_labels)

    # Generate Ideograms (ring outline)
    idata, ishapes, ideo_ends = genIdeograms(pi_vector, digit_labels, digit_counts, ribbon_width)

    # Generate Ribbons (lines connecting the outer rings)
    rshapes = genRibbons(pi_vector, ideo_ends, ribbon_width, digit_labels)
    # rshapes = []

    # Get annotations (labels around the edge, central Pi symbol, ...)
    annotations = genAnnotations(digit_labels, ideo_ends)

    # Generate output figure
    genFigure(idata, ishapes + rshapes, annotations)


