### Chord Diagram - Pi

- Script to generate a chord diagram based on the digits of Pi
    - i.e. 1st ribbon from 3 to 1, 2nd from 1 to 4, 3rd from 4 to 1, ...
- Project page: https://gitlab.com/CPUQuaeler/chord-diagram-pi

Example: ![](example_thumb.jpg)

### Requirements

- Python 3.7 or newer: https://www.python.org/downloads/
- pip or easy_install: https://www.google.com/search?q=python+pip
    (should come with your python installation)


### Installation

git clone https://gitlab.com/CPUQuaeler/chord-diagram-pi.git <br />
cd chord-diagram-pi <br />
pip install -r requirements.txt <br />


### Usage

python chord_diagram_pi.py <br />
- generates html with the chord diagram
- file should open in default browser
- to save file, hover of top-right corner of the image and select "Download plot as a png"
- script doesn't accept command line options, to configure it
    - open scrip in an editor
    - modify CONFIG dictionary
    - most relevant parameters: num_digits, color_map, width_mm, height_mm and margins_mm, R_ribb_th


### Notes

- Script was written and tested on Windows 7, but "should" be platform independent
- To add a personal signature, create author.txt in the script directory and insert your name